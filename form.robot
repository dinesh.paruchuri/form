*** Settings ***
Library     SeleniumLibrary
*** Variables ***

*** Keywords ***

*** Test Cases ***
open the url and test
    open browser     https://demoqa.com/automation-practice-form     chrome
    maximize browser window
    Set Selenium Timeout    5s
    sleep   15s
    Input Text    xpath://input[@id='firstName']    Dinesh Sai Teja
    Input Text    xpath://input[@id='lastName']     Paruchuri
    Input Text    xpath://input[@id='userEmail']    dineshsaitjea.p@gmail.com
    Click Element    xpath://label[@for='gender-radio-1']

    Input Text    xpath://input[@id='userNumber']     1234567890
    #Clear Element Text    xpath://input[@id='dateOfBirthInput']
    click element    xpath://input[@id='dateOfBirthInput']
    Select From List By Label    //select[@class='react-datepicker__year-select']    2001
    Select From List By Label    //select[@class='react-datepicker__month-select']    November
    Click Element    //*[@aria-label='Choose Monday, November 26th, 2001']
    sleep   5s
    Wait Until Element Is Visible    xpath://textarea[@id='currentAddress']
    Input Text    xpath://textarea[@id='currentAddress']     PWC Office, Bhubaneswar
    Scroll Element Into View    xpath://input[@id='react-select-4-input']
#    #Textarea Should Contain    xpath://textarea[@id='currentAddress']    hi
    Wait Until Element Is Visible    xpath://input[@id='react-select-3-input']
    Input Text    xpath://input[@id='react-select-3-input']     NCR
    #Click Element    xpath://input[@id='react-select-3-input']    #sUttar Pradesh
    Press Keys    xpath://input[@id='react-select-3-input']    ENTER
    Press Keys    xpath://input[@id='react-select-3-input']    RETURN
    sleep   15s
    Wait Until Element Is Visible    xpath://label[contains(text(),'Sports')]
    Click Element    xpath://label[contains(text(),'Sports')]
    sleep   3s


    Wait Until Element Is Visible    xpath://input[@id='react-select-4-input']
    Input Text    xpath://input[@id='react-select-4-input']     Delhi
    #Click Element    xpath://input[@id='react-select-3-input']    #sUttar Pradesh
    Press Keys    xpath://input[@id='react-select-4-input']    ENTER
    Press Keys    xpath://input[@id='react-select-4-input']    RETURN
    sleep   15s
#    #Click Button    xpath://input[@id='uploadPicture']
#    Wait Until Element Is Visible    xpath://input[@id='uploadPicture']
#    Click Element    xpath://input[@id='uploadPicture']
#    #Choose File    xpath://input[@id='uploadPicture']    C:\Users\dparuchuri002\Pictures\Saved Pictures\sample.jpg
    sleep   15s
    Wait Until Element Is Visible    xpath://input[@id='subjectsInput']
    Click Element    xpath://input[@id='subjectsInput']
    Input Text    xpath://input[@id='subjectsInput']     Maths,Physics,Chemistry
    sleep  3s
    Capture Page Screenshot     C:\Users\dparuchuri002\PycharmProjects\Task2\Assignment\Screenshot.jpg

   sleep   5s
   Close Browser
